#!/usr/bin/env python


from setuptools import setup


setup(name='python-acm-5178-test',
      version='1.0.2',
      description='Tests for the 1995 ACM Scholastic Programming Contest Finals : Problem B : Time and Motion.',
      author='Pedro Salgado',
      author_email='steenzout@ymail.com',
      url='https://bitbucket.org/steenzout/acm-5178',
      classifiers=[
        'Programming Language :: Python :: 2.7',],
      scripts=[
          'scripts/test-acm-5178',
          'scripts/test-acm-5178-input'])
