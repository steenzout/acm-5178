SHELL = /usr/bin/env bash

init:
	virtualenv env
	bash setup.sh

clean:
	rm -rf build
	rm -rf env
	rm -f *.deb
	rm -rf dist/
	rm -rf *.egg-info/

deb :
	fpm -s python --python-bin /usr/bin/python -t deb setup.py

rpm :
	python setup.py bdist_rpm --python=/usr/bin/python

test :
	python2.7 -m unittest discover -s test -p '*_test.py'
